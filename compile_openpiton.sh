# export PATH="${PATH}:/home/tutel/Projects/MEEP/llvm-EPI-0.7-release-toolchain-cross/bin"
# export PATH="${PATH}:/home/msarkisla/llvm-EPI-0.7-release-toolchain-cross/bin"

# this assumes matmul folder is in meep-sa-tests/isa/sa_nn_benchmarks/ of 
# git clone https://gitlab.bsc.es/meep/meep-design-verification/meep-sa-tests.git
# git checkout dev

#CFILE name of the .c file
CFILE="meep_matmul"  #name of the C file -mabi=lp64 
OPTIMIZATION="2"   #optimziation level(0,1,2,3) (for higher optimization than 0 variables must be volatile)
EXT="openpiton_"

echo "compiling $CFILE code as $EXT"

rm $EXT$CFILE$OPTIMIZATION*
clang -DFLOAT -DCUSTOM -DCYCLES -DPRINT -march=rv64g -static -mcmodel=medany -nostdlib -nostartfiles -mepi -mmeep-sa -mllvm -no-epi-broadcast-folding -I../../../env -I../../../env/p -I../../macros/scalar -Tlink_ariane.ld -DACME_MODE -DPRINTF ../../lib/syscalls.c $CFILE.c  test_wrapper_sa_nn.S -o $EXT$CFILE$OPTIMIZATION.exe 

if ! test -f "$EXT$CFILE$OPTIMIZATION.exe"; then
    echo "$EXT$CFILE$OPTIMIZATION.exe does not exists ,exiting ."
	exit 1
fi

llvm-objdump --mattr=+m,+f,+d,+c,+a,+meep-sa  -D -g  $EXT$CFILE$OPTIMIZATION.exe   > $EXT$CFILE$OPTIMIZATION.disassemble.all

riscv64-unknown-elf-objcopy -O binary -j .text -j  .text.startup $EXT$CFILE$OPTIMIZATION.exe $EXT$CFILE$OPTIMIZATION.hex 
riscv64-unknown-elf-objdump -d $EXT$CFILE$OPTIMIZATION.exe > $EXT$CFILE$OPTIMIZATION.idump
riscv64-unknown-elf-objdump --disassembler-options=no-aliases,numeric -D -g  $EXT$CFILE$OPTIMIZATION.exe > $EXT$CFILE$OPTIMIZATION.all
riscv64-unknown-elf-objcopy -O binary  --remove-section  .text --remove-section .text.startup --remove-section  interp  --remove-section .plt  --strip-debug  $EXT$CFILE$OPTIMIZATION.exe $EXT$CFILE$OPTIMIZATION.mem
riscv64-unknown-elf-objdump  -s  -b binary --adjust-vma=$data_mem_start_adress  $EXT$CFILE$OPTIMIZATION.mem  > $EXT$CFILE$OPTIMIZATION.mdump
