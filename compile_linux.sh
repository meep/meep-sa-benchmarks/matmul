# This script compiles the c code for riscv OS
# export PATH="${PATH}:/home/tutel/Projects/MEEP/llvm-EPI-0.7-release-toolchain-cross/bin"

#optimization level can be 0 1 2 3 (-O0 O1 O2 O3 )
MABI="lp64d" 
data_mem_start_adress="0x4000" #data memory start adress, default is 16K = 16384 = 0x4000 ,1 adress holds 8 bits of data
#CFILE name of the .c file
CFILE="meep_matmul"  #name of the C file
OPTIMIZATION="2"   #optimziation level(0,1,2,3) (for higher optimization than 0 variables must be volatile)
EXT="linux_float_"

echo "compiling $CFILE code as $EXT"

rm $EXT$CFILE$OPTIMIZATION*
clang -DFLOAT -DCYCLES -DPRINT -mepi -mmeep-sa -mllvm -no-epi-broadcast-folding $CFILE.c -o $EXT$CFILE$OPTIMIZATION.exe 
#clang -DFLOAT -DCYCLES -DPRINT -mabi=lp64 -mcmodel=medany -mepi -mmeep-sa -mllvm $CFILE.c -o $EXT$CFILE$OPTIMIZATION.exe 

if ! test -f "$EXT$CFILE$OPTIMIZATION.exe"; then
    echo "$EXT$CFILE$OPTIMIZATION.exe does not exists ,exiting ."
	exit 1
fi

EXT="linux_custom_"
echo "compiling $CFILE code as $EXT"

clang -DFLOAT -DCYCLES -DPRINT -mepi -mmeep-sa -mllvm -no-epi-broadcast-folding $CFILE.c -o $EXT$CFILE$OPTIMIZATION.exe 
if ! test -f "$EXT$CFILE$OPTIMIZATION.exe"; then
    echo "$EXT$CFILE$OPTIMIZATION.exe does not exists ,exiting ."
	exit 1
fi