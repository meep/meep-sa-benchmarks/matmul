# Mat-MAC (Matrix multiplication and accumulation)

This repository implements the  ```bias[1][n] + input[1][m] * weight [m][n] = result [1][n] ; ``` bfloat16 Matrix multiplication and accumulation function. Since SA-NN has a 16 element size limit n and m must be < 16.


## Generate a random input,bias and weight test vector

```
pip install  -r requirements.txt
python generate_random_test_vector.py
```

This generates, 
* wght.mem, out.mem, in_bs.mem  that are hexadecimal values for weight, result, and input+bias respetively. This is generated for debugging purposes. 
* matrix_values.h header file. C header contains randomly generated input, bias and weight and result values. Both float32 and bfloat16 values are generated to compare with golden model. Outputs are only used for debugging. Bfloat16 represented as uint16 to eliminate any library dependencies.     

## Compile the test code

### compile_x86.sh
This compiles the C code for x86 OS (needs to run inside the x863 computer)



## Run in on the emulator

## Run it on the spice simulation

