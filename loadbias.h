// loads (mem to scrachpad) 16x16 bit bias elements 

void loadbias0  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.0, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias1  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.1, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias2  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.2, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias3  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.3, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias4  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.4, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias5  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.5, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias6  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.6, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias7  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.7, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias8  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.8, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias9  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.9, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias10  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.10, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias11  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.11, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias12  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.12, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias13  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.13, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias14  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.14, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias15  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.15, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias16  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.16, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias17  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.17, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias18  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.18, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias19  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.19, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias20  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.20, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias21  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.21, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias22  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.22, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias23  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.23, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias24  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.24, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias25  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.25, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias26  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.26, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias27  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.27, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias28  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.28, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias29  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.29, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}

void loadbias30  (  uint16_t *bi ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) biassize bias elements (size of the each element is 16 bit)
      "sa.load1d1.1.1 sa.30, (%[bi])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [bi]"r"(bi)   /* register biass*/
	  :  // no clobbers
	 );
}



void (*loadbias[])(uint16_t *bi) = {
loadbias0,loadbias1,loadbias2,loadbias3,loadbias4,loadbias5,loadbias6,loadbias7,loadbias8,loadbias9,\
loadbias10,loadbias11,loadbias12,loadbias13,loadbias14,loadbias15,loadbias16,loadbias17,loadbias18,loadbias19,\
loadbias20,loadbias21,loadbias22,loadbias23,loadbias24,loadbias25,loadbias26,loadbias27,loadbias28,loadbias29,\
loadbias30\
};
