// loads (mem to scrachpad) 16x16 16 bit weight elements 

void loadweight0  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.0, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight1  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.1, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight2  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.2, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight3  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.3, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight4  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.4, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight5  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.5, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight6  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.6, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight7  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.7, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight8  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.8, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight9  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.9, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight10  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.10, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight11  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.11, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight12  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.12, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight13  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.13, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight14  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.14, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight15  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.15, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight16  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.16, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight17  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.17, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight18  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.18, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight19  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.19, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight20  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.20, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight21  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.21, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight22  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.22, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight23  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.23, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight24  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.24, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight25  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.25, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight26  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.26, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight27  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.27, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight28  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.28, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight29  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.29, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}

void loadweight30  (  uint16_t w[8][32] ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) weightsize weight elements (size of the each element is 16 bit)
      "sa.load2d0x1.1.1 sa.30, (%[w])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [biptr]"=&r"(biptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [w]"r"(w)   /* register weight*/
	  :  // no clobbers
	 );
}



void (*loadweight[])(uint16_t w[8][32]) = {
loadweight0,loadweight1,loadweight2,loadweight3,loadweight4,loadweight5,loadweight6,loadweight7,loadweight8,loadweight9,\
loadweight10,loadweight11,loadweight12,loadweight13,loadweight14,loadweight15,loadweight16,loadweight17,loadweight18,loadweight19,\
loadweight20,loadweight21,loadweight22,loadweight23,loadweight24,loadweight25,loadweight26,loadweight27,loadweight28,loadweight29,\
loadweight30\
};
