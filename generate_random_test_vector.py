import numpy as np
import tensorflow as tf
import math
import struct
import numpy.matlib

# way the weight loads is little complicated
# It is a little complicated. Sample shortint weight_mem_s [8][31:0] array is given below as 4hex values for each element
#  3f4e bf27 40ce 40e2 40be c14a bfe8 3f76 3f8c becf bebb 410f be89 c0f4 c098 beab bfd3 4162 c127 3f5a c06a c109 3f41 3f89 c127 3e86 4020 be82 3e8c 4038 3eca bf4f
#  bec2 405e 4159 c066 bf37 bf69 3e90 4025 bf20 3f9e 4121 4052 bf3c 416b 40ba 3fbf 4011 3e83 3ea0 c028 3f48 3f6f 40bb bf14 3f72 beb4 bf4e c098 bfd2 bf1c c015 3f5f
#  3f5c 3ea1 3f8a 40e2 c07e 413b 3ff3 bf7d bf2e bf2b 3f26 4062 c010 bf5d bfd4 bffa bf7d bee9 4176 bea6 bf9b 3fcf c0a1 c139 4027 4008 be95 3f9b 401f 4127 c126 c0d3
#  3fe1 bf47 410f c01d bec1 3f0a c0e8 beb1 c0cf c0b0 4018 40ee 4071 3fa2 bed5 40b9 c01b 40cc 3ea3 beec 3f05 3f41 3f6b 3fff c091 bf9b c0a0 416e 408f c13a c080 c078
#  40d9 bec8 4060 c0ce bf93 bf1b 3ef5 c071 4164 c07c c053 3ef9 bf5d bee8 bf15 4167 3fb6 3efd 4136 40cf 3f3b 3f3a 3ffd c0cb c0cd bffe bf10 c093 406f c12c be92 40e3
#  c08f 3f1c c05d c103 4026 40af bfd3 3f0a c093 c129 c108 3fe0 3ebb bec0 416d c12f bf72 c0ae 3f89 c06e c03d 4079 4104 3e84 40e5 bf78 4174 3fd0 bf8c 4094 bfa6 c0da
#  bf48 bf4e c096 3ff3 c02d bf2c 3fe0 c04c c15b bee6 4015 3ed6 3ed3 3fe3 3f4a 4073 c0d8 c079 c06c 3f8e 3fd3 3f9f 4026 416f c02b 3e9e bec8 3e85 3f19 c13c 3f3d 40fc
#  bfb3 bf8e 40c5 bee1 3eb2 bf85 3ffb c128 c05f 3ec6 c029 3f82 bf8b 4162 c11d c161 404b 3fa2 c0a0 bed9 4039 3f6f bf90 3eab c0e4 c05e 3fb3 3f73 bf8d bf4a 3f84 3f6f
#  At a single time we can load 512 bits (32x16bits) to SA-NN, which corresponds to single weight_mem_s row. Each element(column) of a single row is correspond to different row/column  in an imaginary 4x8 grid. 4x8 grid is shown below.
#  
#  |0 ||1 ||2 ||3 ||16||17||18||19|
#  |4 ||5 ||6 ||7 ||20||21||22||23|
#  |8 ||9 ||10||11||24||25||26||27|
#  |12||13||14||15||28||29||30||31|
#  And this is how different rows of weight_mem_s (each row correspond to above 4x8 grid) are put as 16x16 NN layer. Each number is a 4x8 grid:
#  For the first row 3f4e is the |0 | and bf4f is the |31|  of the |  0  | 4x8 matrix.
#  
#  |  0  ||  2  |
#  |  1  ||  3  |
#  |  4  ||  6  |
#  |  5  ||  7  |
#  This way of putting the data in SA allows us to start calculations early without waiting all weights to load.
# read weight extract interface and load data
# weightname has a different load method. each row contains interfacesize bits, and each number contains (2 bytes,16 bits), so each row has interfacesize/16 floating point numbers
# Since weightname contains float data for all cells in PE Network, it mus contain PE_side*PE_side numbers. So, row number is determined as (PE_side*PE_side)/(interfacesize/16) 
# each INT_side float numbers first tuned to a rectange block which in turn then added together to turned into square block. For example if INT_side=32 it must be turned to 8x8 block.
# First INT_side=32 numbers are turned into 4x8 block. Then, two 4x8 blocks are added on top to generate 8x8 square block. If INT_side its already square (like 64), 8x8 block is directly generated.
# then using 8x8 blocks 16x16 block is generated. First columns are complated, then rows.
# So a INT_side=32 will be loaded in PE_side=16xPE_side=16 Network as 
# |  0  ||  2  |
# |  1  ||  3  |
# |  4  ||  6  |
# |  5  ||  7  |
# |  x  | is the generated 4x8 block using INT_side=32 numbers, it is combined to 8x8 block and then loaded by parts
# single block loaded as:
# |0 ||1 ||2 ||3 ||16||17||18||19|
# |4 ||5 ||6 ||7 ||20||21||22||23|
# |8 ||9 ||10||11||24||25||26||27|
# |12||13||14||15||28||29||30||31|
# INT_side is seperated as 4x4 square blocks, added side by side to generate 4x8 blocks
# then 4x8 blocks added to bottom to generate 8x8 blocks
# this in turn then generates 16x16 Pe network
def reorder_weight (weightmatrix):
    # constants
    NN_side= 1
    PE_side = 16
    INT_side = 32 # number of integers in one interface
    LOAD_side = 2**math.floor(math.log2(math.floor(math.sqrt(INT_side)))) # LOAD_side is the row number of the single block 
    LOAD_size = LOAD_side*LOAD_side
    # square side is the lenght of the square block
    SQUARE_side = INT_side/LOAD_side
    
    oldWeightMatrix = weightmatrix ;
    weightMatrix_temp = np.arange(PE_side*PE_side, dtype=object)
    weightMatrix_temp = weightMatrix_temp.reshape((PE_side, PE_side))
    Colcounter = 0
    Rowcounter = 0
    counter = 0
    squarecounter = 0
    #this generates PE_sidexPE_side weightMatrix_temp matrix from read data (oldWeightMatrix)
    for i in range(len(oldWeightMatrix)):
        for j in range(math.floor(INT_side/LOAD_size)): # how many matrices one interface corresponds to
            PE_side_group_matrix = oldWeightMatrix[i,j*LOAD_size:j*LOAD_size+LOAD_size]                   
            PE_side_group_matrix = np.reshape(PE_side_group_matrix, (LOAD_side, LOAD_side))
            weightMatrixrowstart = math.floor(Rowcounter + (counter%SQUARE_side)) ;
            weightMatrixcolstart = math.floor(Colcounter +j*LOAD_side);
            # weightMatrix[:,:+]  = PE_side_group_matrix;  
            #print ( weightMatrixrowstart ,weightMatrixcolstart )
            weightMatrix_temp[weightMatrixrowstart:weightMatrixrowstart+LOAD_side,weightMatrixcolstart:weightMatrixcolstart+LOAD_side, ] =  PE_side_group_matrix          
        counter = counter +LOAD_side;         
        if (counter % SQUARE_side == 0):
            Colcounter = Colcounter +SQUARE_side ;
        if (Colcounter % PE_side == 0 and Colcounter >0):
            Colcounter = 0
            Rowcounter = Rowcounter + SQUARE_side           
        if (counter + LOAD_side > NN_side * PE_side * 2):
            break
    
    return weightMatrix_temp
    
# Generate random matrices with bfloat16 values
def generate_random_matrix_multiplication(n, m):
    input_vec = tf.random.uniform((1, n),minval=-10, maxval=10 ,dtype=tf.bfloat16)
    # this generates random memory file 
    weight_mem = tf.random.uniform((8, 32),minval=-10, maxval=10 , dtype=tf.bfloat16)
    # this gets the memory file to geterate 16x16 matrix     
    weight_vec = reorder_weight (weight_mem)
    # now clice it to n by m 
    weight_vec = weight_vec[:n, :m]
    weight_vec = tf.convert_to_tensor(weight_vec,dtype=tf.bfloat16 ) 
    
    bias_vec = tf.random.uniform((1, m),minval=-10, maxval=10 , dtype=tf.bfloat16)

    # Print the matrices, the result, and the additional value
    print("input Matrix :")
    print(input_vec)
    print("weight  mem:")
    print(weight_mem)    
    print("weight  matrix:")
    print(weight_vec)
    print("bias vec:")
    print(bias_vec)

    return input_vec,weight_vec,bias_vec, weight_mem

# take bloat16 and return its same byte int representation
def bfloattoint (bfloat_val):
    float_var = tf.cast(bfloat_val,tf.float32)
    byte_var = struct.pack('f', float_var);
    integer_var = struct.unpack('H', byte_var[2:4] )[0];
    return integer_var

# write 6 matrix values as c_header
def write_as_cheader (input_v,weight_v,bias_v , weight_m , result_f, result_nn):
    # Set the filename for the header file
    f = open("matrix_values.h", "w")

    rows, cols = weight_v.shape
    
    # Write matrices, result, and additional value to .h file
    f.write(" ".join(["#define isize", str(rows),"\n" ]) )
    f.write(" ".join(["#define bsize ", str(cols),"\n" ]))

    f.write("// Input_f \n")
    f.write("static float input_f[isize] = {")
    f.write(", ".join([str(val) for val in input_v.numpy().flatten()]))
    f.write("};\n\n")
    
    f.write("// Input_nn \n")
    f.write("static uint16_t input_nn[isize] = {")
    f.write(", ".join([str(bfloattoint(val)) for val in input_v.numpy().flatten()]))
    f.write("};\n\n")
    
    f.write("// Weight_f \n")        
    f.write("static float weight_f[isize] [bsize] = {\n")
    for row in weight_v.numpy():
        f.write("    {")
        f.write(", ".join([str(val) for val in row]))
        f.write("},\n")
    f.write("};\n")
    
    f.write("// Weight_nn \n")        
    f.write("static uint16_t   weight_nn[8] [32] = {\n")
    for row in weight_m.numpy():
        f.write("    {")
        f.write(", ".join([str(bfloattoint(val)) for val in row]))
        f.write("},\n")
    f.write("};\n")
    
    
    f.write("// Bias_f \n")
    f.write("static float bias_f[bsize] = {")
    f.write(", ".join([str(val) for val in bias_v.numpy().flatten()]))
    f.write("};\n\n")
    
    f.write("// Bias_nn \n")
    f.write("static uint16_t bias_nn[isize] = {")
    f.write(", ".join([str(bfloattoint(val)) for val in bias_v.numpy().flatten()]))
    f.write("};\n\n")
    
    f.write("// Output_f \n")
    f.write("volatile float output_f[bsize] = {")
    f.write(", ".join(["0" for val in bias_v.numpy().flatten()]))
    f.write("};\n\n")
    
    f.write("// Output_nn \n")
    f.write("volatile uint16_t output_nn[bsize] = {")
    f.write(", ".join(["0" for val in bias_v.numpy().flatten()]))
    f.write("};\n\n")
    
    f.write("// expected_f \n")
    f.write("static float expected_f[bsize] = {")
    f.write(", ".join([str(val) for val in result_f.numpy().flatten()]))
    f.write("};\n\n")
    
    f.write("// expected_nn \n")
    f.write("static uint16_t expected_nn[bsize] = {")
    f.write(", ".join([str(bfloattoint(val)) for val in result_nn.numpy().flatten()]))
    f.write("};\n\n")
    
    f.write("static float expected_nn_bfloat[bsize] = {")
    f.write(", ".join([str(val) for val in result_nn.numpy().flatten()]))
    f.write("};\n\n")
    
    f.close()
    
    f = open("in_bs.mem", "w")
    
    f.write("\n".join([str(hex(bfloattoint(val)))[2:] for val in input_v.numpy().flatten()]))
    f.write("\n")
    f.write("\n".join([str(hex(bfloattoint(val)))[2:] for val in bias_v.numpy().flatten()]))
    
    f.close()

    f = open("wght.mem", "w")
    
    for row in weight_m.numpy():
        f.write(" ".join([str(hex(bfloattoint(val)))[2:] for val in row]))
        f.write("\n")
    
    f.close()


def sa_nn_float32 (input_v,weight_v,bias_v):

    # convert matrices to  bfloat16  to float32
    weightMatrix = tf.cast(weight_v,tf.float32 ) 
    #print("weightMatrix float32:")
    #print(weightMatrix )  
    inputMatrix = tf.cast(input_v,tf.float32 ) 
    biasMatrix = tf.cast(bias_v,tf.float32 ) 
    
    rows, cols = weightMatrix.shape
    result =  tf.Variable(tf.convert_to_tensor(np.matlib.repmat(0, 1,cols),dtype=inputMatrix.dtype ) )
    temp=tf.Variable(result)
        
    for col in range(cols):
        # Add the bias to the result
        result[0,col].assign(biasMatrix[0,col]) ;
        for row in range(rows):
            temp[0,col].assign( result[0,col] + weightMatrix [row,col] *   inputMatrix [0,row]) ;
            result[0,col].assign( temp[0,col]) ;
    
    
    
    #result = sa_nn (inputMatrix,weightMatrix,biasMatrix)
    return result 

def sa_nn (input_v,weight_v,bias_v):

    # Perform matrix multiplication (tf.matmul algorithm is different than adding them one by one,)
    # so result was different than the firmware
    #result = tf.matmul(input_v, weight_v)  
    #print("result matmul:")
    #print (result);
    
    # this was also giving different results with the firmware
    #rows, cols = weight_v.shape
    #result =  tf.Variable(tf.convert_to_tensor(np.matlib.repmat(0, 1,cols),dtype=input_v.dtype ) )
    #temp=tf.Variable(result)
    #    
    #for col in range(cols):
    #    # Add the bias to the result
    #    result[0,col].assign(bias_v[0,col]) ;
    #    for row in range(rows):
    #        temp[0,col].assign( result[0,col] + weight_v [row,col] *   input_v [0,row]) ;
    #        result[0,col].assign( temp[0,col]) ;
    #        
    #print("result matmul:")
    #print (result)
    
    # convert matrices to  bfloat16  to float32
    weightMatrix = tf.cast(weight_v,tf.float32 ) 
    #print("weightMatrix float32:")
    #print(weightMatrix )  
    inputMatrix = tf.cast(input_v,tf.float32 ) 
    biasMatrix = tf.cast(bias_v,tf.float32 ) 
    
    rows, cols = weight_v.shape
    result =  tf.Variable(tf.convert_to_tensor(np.matlib.repmat(0, 1,cols),dtype=tf.float32  ) )
    temp32 =  tf.Variable(tf.convert_to_tensor(np.matlib.repmat(0, 1,cols),dtype=tf.float32 ) )
    temp16 =  tf.Variable(tf.convert_to_tensor(np.matlib.repmat(0, 1,cols),dtype=tf.bfloat16 ) )

    
    for col in range(cols):
        # Add the bias to the result
        result[0,col].assign(biasMatrix[0,col]) ;
        for row in range(rows):
            temp32[0,col].assign( result[0,col] + weightMatrix [row,col] *   inputMatrix [0,row]) ;
            temp16[0,col].assign(tf.cast(temp32[0,col],tf.bfloat16 )   ) ;
            result[0,col].assign(tf.cast(temp16[0,col],tf.float32 )   ) ;
            
    #print("result matmul:")
    #print (result)
            
    return result

# Set the values of n and m
isize = 16
bsize = 16

# Generate random matrix multiplication example
input_vec,weight_vec,bias_vec, weight_mem = generate_random_matrix_multiplication(isize, bsize)

result_nn = sa_nn (input_vec,weight_vec,bias_vec )

print("result bloaf16:")
print (result_nn);

f = open("out.mem", "w")
f.write("\n".join([str(hex(bfloattoint(val)))[2:] for val in result_nn.numpy().flatten()]))
f.close()
    
    
result_f =  sa_nn_float32 (input_vec,weight_vec,bias_vec )

print("result float32:")
print (result_f);
    
write_as_cheader (input_vec,weight_vec,bias_vec, weight_mem , result_f, result_nn )

