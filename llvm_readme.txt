Dowload llvm-EPI-0.7-development-toolchain-cross from https://admin.hca.bsc.es/epi/ftp/?C=M;O=D 
Cross compiler for hosts x86-64. Targets RISC-V, 
Native compiler for RISC-V host. Targets RISC-V,

If you have access to the arriesgado cluster, the (native) compiler is already deployed there. 
export PATH="${PATH}:~/const_insts/llvm-EPI-0.7-release-toolchain-cross/bin"


#optimization level can be 0 1 2 3 (-O0 O1 O2 O3 )
MABI="lp64d" 
data_mem_start_adress="0x4000" #data memory start adress, default is 16K = 16384 = 0x4000 ,1 adress holds 8 bits of data
#CFILE name of the .c file
-----------example_genann---------------
CFILE="SAD"  #name of the C file
OPTIMIZATION="0"   #optimziation level(0,1,2,3) (for higher optimization than 0 variables must be volatile)
MARCH="rv64imafd"  #

# riscv64-unknown-elf-gcc -mabi=$MABI -g0 -O$OPTIMIZATION -march=$INST_SET  -Wl,--no-relax -nostartfiles start.S $CFILE.c -T link.ld -o _$OPTIMIZATION.exe -lm

rm $CFILE$OPTIMIZATION*
clang -mmeep-sa -Wl,--no-relax -O$OPTIMIZATION -nostartfiles -march=$MARCH start.S $CFILE.c -T link.ld -o $CFILE$OPTIMIZATION.exe 
riscv64-unknown-elf-objcopy -O binary -j .text -j  .text.startup $CFILE$OPTIMIZATION.exe $CFILE$OPTIMIZATION.hex 
riscv64-unknown-elf-objdump -d $CFILE$OPTIMIZATION.exe > $CFILE$OPTIMIZATION.idump
riscv64-unknown-elf-objdump --disassembler-options=no-aliases,numeric -D -g  $CFILE$OPTIMIZATION.exe > $CFILE$OPTIMIZATION.all
riscv64-unknown-elf-objcopy -O binary  --remove-section  .text --remove-section .text.startup --remove-section  interp  --remove-section .plt  --strip-debug  $CFILE$OPTIMIZATION.exe $CFILE$OPTIMIZATION.mem
riscv64-unknown-elf-objdump  -s  -b binary --adjust-vma=0xa000  $CFILE$OPTIMIZATION.mem  > $CFILE$OPTIMIZATION.mdump

cd ~/projects/MEEP_llvm/sa_shell_sa_nn/sim/top_module/mockprocessor
./mockprocessor.sh 1 2   

Basic usage

    Enable the MEEP SA extensions passing the flag -meep-sa to clang.
    Use the flag -S (instead of -c) to obtain the assembly in case you want to inspect it.
        Your assembly blocks will be wrapped in APP and NO_APP  comments, so they are easy to spot.
    Please enable optimisation (e.g. -O2) as a way to see that the compiler retains your asm blocks and fills in the placeholders correctly.
    
    
    // Hypothetical example using the systolic array 0.
void my_dot_product(float *in_a, float *in_b, float *out_c, long l) {
  __asm__(
      "sa.setoplen.0.0 x0, %[length]\n" // sets the operational length to "l"
      "sa.load1d0.0.2 sa.0, (%[a])\n"   // loads first operand ("l" elements of
                                        // 32-bit)
      "sa.load1d0.0.2 sa.1, (%[b])\n"   // loads second operand ("l" elements of
                                        // 32 bit)
      "sa.op12.0.1 sa.3, sa.0, sa.1\n"  // performs the operation, leaves result
                                        // in sa.3
      "sa.setopleni.0.0 x0, 1\n"        // sets the operational length 1
      "sa.store1d0.0.2 sa.3, (%[c])\n"  // stores 1 element of 32 bit
      : /* no register outputs */
      : [length] "r"(l), [a] "r"(in_a), [b] "r"(in_b), [c] "r"(out_c)
      : "memory" /* tell the compiler we will update the memory */);
}


In the example above:

    we use __asm__ but it is the same as asm.
    there are no asm-qualifiers
    The AssemblerTemplate is a giant string literal starting with "sa.setoplen.0.0 ..." (don't forget the \n in each line, except possibly the last but it won't harm if you add it anyways)
        Recall that in C/C++ "foo\n" "bar" is the same as "foo\nbar"
    There are no outputs of the block
        We don't output anything in a scalar register in this case
    The input operands are : [length] "r"(l), [a] "r"(in_a), [b] "r"(in_b), [c] "r"(out_c)
        The syntax is a comma-separated list of [name-in-the-asm-template] "constraint" (variable)
    We add a clobber: we tell the compiler this chunk of assembly writes to memory.
