// loads (mem to scrachpad) 16 input elements (size of the each element is 16 bit)

void loadinput0  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.0, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput1  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.1, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput2  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.2, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput3  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.3, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput4  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.4, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput5  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.5, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput6  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.6, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput7  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.7, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput8  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.8, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput9  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.9, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput10  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.10, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput11  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.11, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput12  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.12, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput13  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.13, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput14  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.14, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput15  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.15, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput16  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.16, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput17  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.17, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput18  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.18, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput19  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.19, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput20  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.20, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput21  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.21, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput22  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.22, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput23  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.23, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput24  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.24, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput25  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.25, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput26  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.26, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput27  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.27, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput28  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.28, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput29  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.29, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}

void loadinput30  (  uint16_t *in ) {
	 __asm__ volatile(
	 // loads (mem to scrachpad) inputsize input elements (size of the each element is 16 bit)
      "sa.load1d0.1.1 sa.30, (%[in])"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  : [in]"r"(in)   /* register inputs*/
	  :  // no clobbers
	 );
}



void (*loadinput[])(uint16_t *in) = {
loadinput0,loadinput1,loadinput2,loadinput3,loadinput4,loadinput5,loadinput6,loadinput7,loadinput8,loadinput9,\
loadinput10,loadinput11,loadinput12,loadinput13,loadinput14,loadinput15,loadinput16,loadinput17,loadinput18,loadinput19,\
loadinput20,loadinput21,loadinput22,loadinput23,loadinput24,loadinput25,loadinput26,loadinput27,loadinput28,loadinput29,\
loadinput30\
};
