#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "matrix_values.h"
#ifdef CUSTOM
	#include "loadinput.h"
	#include "loadbias.h"
	#include "loadweight.h"
#endif

// possible flags
// FLOAT : execution in standart float instructions
// CUSTOM : execution in custom meep instructions
// PRINT : print the values
// CYCLES : Calculate and print the cycles 

// x86 : -D FLOAT -D PRINT
// float in TBTK sim: -D FLOAT
// custom in TBTK sim: -D CUSTOM
// float in linux riscv OS: -D FLOAT -D CYCLES -D PRINT
// OPENPİTON EMULATOR: -D FLOAT -D CUSTOM -D CYCLES -D PRINT

float trim_last_16_bits(float value) {
    unsigned int *bits = (unsigned int *)&value;
    *bits &= 0xFFFF0000;
    return value;
}

void matrix_vector_multiply(int inputsize,  int biassize,  float *in,  float *bi,  float w[inputsize][biassize],  volatile float *out) {

    for (int j = 0; j < biassize; j++) {
        out[j] = bi[j];
        for (int i = 0; i < inputsize; i++) {
			out[j] += w[i] [j ] * in[i];
        }
    }
}

#ifdef CUSTOM
void setoplen_input  (  int inputsize) {
	 __asm__ volatile(
	 // sets the input length to "inputsize"
      "sa.setoplen.1.0 x0, %[inputsize]"
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  :  [inputsize]"r"(inputsize)    /* register inputs*/
	  :  // no clobbers
	 );
}

void setoplen_bias  (  int biassize) {
	 __asm__ volatile(
	 // sets the bias length to "biassize"
      "  sa.setoplen.1.1 x0, %[biassize] "
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  :  [biassize]"r"(biassize)    /* register inputs*/
	  :  // no clobbers
	 );
}
 
// Hypothetical example using the systolic array 0.
// __attribute__ ((noinline))   uint16_t *in,  uint16_t *bi,  uint16_t w[8][32]
void  my_nn(  volatile uint16_t *out ) {
	
	/*inptr = in;
    biptr = bi;
	wtptr = w;
	bsize = biassize;
	isize = inputsize;*/
	
	 __asm__ volatile(
	 /*sa.load1d0.1.1 sa.0, (%[in]) ;\
	  sa.load1d1.1.1 sa.1, (%[bi]) ;\
	  sa.load2d0x1.1.1 sa.2, (%[w]) ;\*/
	  
	 // systollic array operation with no activation function
	 // output len = bias len , store the output to memory
      "sa.op13.1.0 sa.3, sa.0, sa.1, sa.2 ;\
	  sa.store1d1.1.1 sa.3, (%[out])      "
	  :  //[isize]"=&r"(isize) , [bsize]"=&r"(bsize), [inptr]"=&r"(inptr), [biptr]"=&r"(biptr), [wtptr]"=&r"(wtptr)  /*  register outputs */
	  :   [out]"r"(out)    /* [in]"r"(in), [bi]"r"(bi), [w]"r"(w) , register inputs*/
	  :  // no clobbers
	 );
  
    //  "sa.store1d1.1.1 sa.3, (%[c])\n"  // stores 1 element of 32 bit
     // : /* no register outputs */
     // : [length] "r"(l), [a] "r"(in_a), [b] "r"(in_b), [c] "r"(out_c)
     // : "memory" /* tell the compiler we will update the memory */

}
#endif
	
int main()
{


	
	#ifdef FLOAT   	
		#ifdef PRINT 	
			printf ("\nFLOATING POINT CALCULATION\n" );
		#endif	
		
		#ifdef CYCLES
		uint64_t start_cycles, start_instret, end_cycles, end_instret;
		__asm__ __volatile__("rdinstret %0" : "=r"(start_instret));
		__asm__ __volatile__("rdcycle %0"   : "=r"(start_cycles));
		#endif
	
	matrix_vector_multiply(isize, bsize,   input_f, bias_f, weight_f, output_f );

		#ifdef CYCLES
		__asm__ __volatile__("rdcycle %0"   : "=r"(end_cycles));
		__asm__ __volatile__("rdinstret %0" : "=r"(end_instret));

		uint64_t cycles  = end_cycles  - start_cycles;
		uint64_t instret = end_instret - start_instret;
		
			#ifdef PRINT
			printf ("\n Performance is cycles=%ld instret=%ld \n",cycles,instret  );
			#endif
		#endif	
	
		#ifdef PRINT 	
		printf ("\nOutputs as floating point values\n" );		
		for (int j = 0; j < bsize; j++) {
			printf ("%f ",output_f[j])  ;
		}		
		printf ("\n\nOutputs as integer values\n" );		
		for (int j = 0; j < bsize; j++) {
			double temp_d = output_f[j] ;		
			long int *temp = &temp_d ;			 
			printf ("%ld ",*temp)  ;
		}
		printf ("\n\n" );	
		#endif

	#endif
	
	#ifdef CUSTOM
		#ifdef PRINT 	
				printf ("\nCUSTOM INSTRUCTION CALCULATION\n" );
		#endif
	
		#ifdef CYCLES
		uint64_t start_cycles2, start_instret2, end_cycles2, end_instret2;
		__asm__ __volatile__("rdinstret %0" : "=r"(start_instret2));
		__asm__ __volatile__("rdcycle %0"   : "=r"(start_cycles2));
		#endif
		
	setoplen_input  (  isize) ; // sets the input length to "inputsize"
	setoplen_bias  ( bsize);  // sets the bias length to "biassize"
	
	loadinput[0]  (  input_nn); // loads (mem to scrachpad) 16 input elements (size of the each element is 16 bit)		
	loadbias[1]  (  bias_nn);  // loads (mem to scrachpad) 16x16 bit bias elements 
	loadweight[2]  (  weight_nn);  // loads (mem to scrachpad) 16x16 16 bit weight elements 

	my_nn		(  output_nn );
		
		#ifdef CYCLES
		__asm__ __volatile__("rdcycle %0"   : "=r"(end_cycles2));
		__asm__ __volatile__("rdinstret %0" : "=r"(end_instret2));

		uint64_t cycles2  = end_cycles2  - start_cycles2;
		uint64_t instret2 = end_instret2 - start_instret2;
		
			#ifdef PRINT
			printf ("\n Performance is cycles=%ld instret=%ld \n",cycles2,instret2  );
			#endif
		#endif	
	
		#ifdef PRINT 			
		printf ("\n\nExpected outputs  as integer values\n" );		
		for (int j = 0; j < bsize; j++) {
			printf ("%u ",expected_nn[j] )  ;
		}
		printf ("\n\n" );
		
		printf ("\n\nOutputs as integer values\n" );		
		for (int j = 0; j < bsize; j++) {
			printf ("%u ",output_nn[j] )  ;
		}
		printf ("\n\n" );	
		#endif	
	
	#endif
	
	
	
    return 0;
}