# This script compiles the c code for tubitak simulator
# export PATH="${PATH}:/home/tutel/Projects/MEEP/llvm-EPI-0.7-release-toolchain-cross/bin"

#optimization level can be 0 1 2 3 (-O0 O1 O2 O3 )
MABI="lp64d" 
data_mem_start_adress="0x4000" #data memory start adress, default is 16K = 16384 = 0x4000 ,1 adress holds 8 bits of data
#CFILE name of the .c file
CFILE="meep_matmul"  #name of the C file
OPTIMIZATION="2"   #optimziation level(0,1,2,3) (for higher optimization than 0 variables must be volatile)
MARCH="rv64imafd"  #
EXT="tbtk_float_"

echo "compiling $CFILE code as $EXT"
rm $EXT$CFILE$OPTIMIZATION*
clang -DFLOAT -mmeep-sa -Wl,--no-relax -O$OPTIMIZATION -nostartfiles -march=$MARCH start.S $CFILE.c -T link.ld -o $EXT$CFILE$OPTIMIZATION.exe 

if ! test -f "$EXT$CFILE$OPTIMIZATION.exe"; then
    echo "$EXT$CFILE$OPTIMIZATION.exe does not exists ,exiting ."
	exit 1
fi

riscv64-unknown-elf-objcopy -O binary -j .text -j  .text.startup $EXT$CFILE$OPTIMIZATION.exe $EXT$CFILE$OPTIMIZATION.hex 
riscv64-unknown-elf-objdump -d $EXT$CFILE$OPTIMIZATION.exe > $EXT$CFILE$OPTIMIZATION.idump
riscv64-unknown-elf-objdump --disassembler-options=no-aliases,numeric -D -g  $EXT$CFILE$OPTIMIZATION.exe > $EXT$CFILE$OPTIMIZATION.all
riscv64-unknown-elf-objcopy -O binary  --remove-section  .text --remove-section .text.startup --remove-section  interp  --remove-section .plt  --strip-debug  $EXT$CFILE$OPTIMIZATION.exe $EXT$CFILE$OPTIMIZATION.mem
riscv64-unknown-elf-objdump  -s  -b binary --adjust-vma=$data_mem_start_adress  $EXT$CFILE$OPTIMIZATION.mem  > $EXT$CFILE$OPTIMIZATION.mdump

EXT="tbtk_custom_"

echo "compiling $CFILE code as $EXT"
rm $EXT$CFILE$OPTIMIZATION*
clang -DFLOAT -mmeep-sa -Wl,--no-relax -O$OPTIMIZATION -nostartfiles -march=$MARCH start.S $CFILE.c -T link.ld -o $EXT$CFILE$OPTIMIZATION.exe 

if ! test -f "$EXT$CFILE$OPTIMIZATION.exe"; then
    echo "$EXT$CFILE$OPTIMIZATION.exe does not exists ,exiting ."
	exit 1
fi

riscv64-unknown-elf-objcopy -O binary -j .text -j  .text.startup $EXT$CFILE$OPTIMIZATION.exe $EXT$CFILE$OPTIMIZATION.hex 
riscv64-unknown-elf-objdump -d $EXT$CFILE$OPTIMIZATION.exe > $EXT$CFILE$OPTIMIZATION.idump
riscv64-unknown-elf-objdump --disassembler-options=no-aliases,numeric -D -g  $EXT$CFILE$OPTIMIZATION.exe > $EXT$CFILE$OPTIMIZATION.all
riscv64-unknown-elf-objcopy -O binary  --remove-section  .text --remove-section .text.startup --remove-section  interp  --remove-section .plt  --strip-debug  $EXT$CFILE$OPTIMIZATION.exe $EXT$CFILE$OPTIMIZATION.mem
riscv64-unknown-elf-objdump  -s  -b binary --adjust-vma=$data_mem_start_adress  $EXT$CFILE$OPTIMIZATION.mem  > $EXT$CFILE$OPTIMIZATION.mdump
